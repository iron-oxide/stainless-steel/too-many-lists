# too-many-lists

## Install

Install [rust](https://www.rust-lang.org):

    curl https://sh.rustup.rs -sSf | sh

The rust development environment including `rustup`, `cargo`, and `rustc` is all installed into `~/.cargo/bin`. As such it is generally added to the `PATH` environment variable.
