// first.rs

use std::mem;

pub struct List {
    head: Link,
}

enum Link {
    Empty,
    More(Box<Node>),
}

struct Node {
    elem: i32,
    next: Link,
}

impl List {
    pub fn new() -> Self {
        List { head: Link::Empty }
    }

    pub fn push(&mut self, elem: i32) {
        let new_node = Box::new(Node {
            elem: elem,
            // mem::replace ensures the value replacement is valid
            next: mem::replace(&mut self.head, Link::Empty),
        });

        // Replacement value that is required
        self.head = Link::More(new_node);
    }

    pub fn pop(&mut self) -> Option<i32> {
        // pattern matching
        match mem::replace(&mut self.head, Link::Empty) {
            Link::Empty => {
                None
            }
            Link::More(boxed_node) => {
                let node = *boxed_node; // pull the node out of the box
                self.head = node.next;  // now take it apart
                Some(node.elem)
            }
        }
    }
}

/** Drop (rusts Destructor)
 *
 * Needed to prevent unbounded recursion of Box.
 *
 */
impl Drop for List {
    fn drop(&mut self) {
        let mut cur_link = mem::replace(&mut self.head, Link::Empty);
        // Do untill patern does not match
        while let Link::More(mut boxed_node) = cur_link {
            // boxed_node goes out of scope
            // `next` field set to Link::Empty
            // This way there is no unbounded recursion
            cur_link = mem::replace(&mut boxed_node.next, Link::Empty);
        }
    }
}

#[cfg(test)]
mod test {
    use super::List;
    #[test] // new namespace
    fn basics() {
        let mut list = List::new();

        // Empty case
        assert_eq!(list.pop(), None);

        // Populate
        list.push(1);
        list.push(2);
        list.push(3);

        // Check removal
        assert_eq!(list.pop(), Some(3));
        assert_eq!(list.pop(), Some(2));

        // Push More!!!
        list.push(4);
        list.push(5);

        // Check removal
        assert_eq!(list.pop(), Some(5));
        assert_eq!(list.pop(), Some(4));

        // Check exhaustion
        assert_eq!(list.pop(), Some(1));
        assert_eq!(list.pop(), None);
    }
}
